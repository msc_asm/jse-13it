package ru.gasanov.tm.repository;

import ru.gasanov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class TaskRepository {

    private List<Task> tasks = new ArrayList<>();
    private final AtomicLong idGenerator = new AtomicLong();

    public void save(Task task) {
        if (task.getId() == null ){
            //pass
        } else{
            task.setId(idGenerator.incrementAndGet());
            tasks.add(task);
        }
    }

    public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        return task;
    }

    public void clear() {
        tasks.clear();
    }

    public List<Task> findAll() {
        return tasks;
    }

    public Task create(final String name, String description){
        final Task task = new Task(name);
        task.setName(name);
        task.setDescription(description);
        tasks.add(task);
        return task;
    }

    public Task update(final Long id, final String name, String description){
        final Task task = findById(id);
        if (task == null) return null;
        task.setId(id);
        task.setDescription(description);
        task.setName(name);
        return task;
    }

    public Task findByIndex(int index){
        if  (index < 0 || index > tasks.size() - 1 ) return null;
        return tasks.get(index);
    }

    public Task findByName(final String name){
        if (name == null || name.isEmpty()) return null;
        for (final Task task: tasks){
            if (task.getName().equals(name)) return task;
        }
        return null;
    }

    public Task removeByName(final String name){
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task removeById(final Long id){
        final Task task = findById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task removeByIndex(final int index){
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task findById(final Long id){
        if (id == 0) return null;
        for (final Task task: tasks) {
            if(task.getId().equals(id)) return task;
        }
        return null;
    }

    public List<Task> findByProjectId(Long projectId) {
        List<Task> result = new ArrayList<>();
        for (Task task : tasks) {
            if (Objects.equals(task.getProjectId(), projectId)) {
                result.add(task);
            }
        }
        return result;
    }

    public int size() {
        return tasks.size();
    }


    public List<Task> findAddByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }


    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        for (final Task task: tasks) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if(task.getId().equals(id)) return task;
        }
        return null;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: findAll()) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) result.add(task);
        }
        return result;
    }

    public List<Task> findByUserId(Long UserId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: findAll()) {
            if (task.getUserId() == null) continue;
            if (task.getUserId().equals(UserId)) result.add(task);
        }
        return result;
    }
}