package ru.gasanov.tm.repository;

import ru.gasanov.tm.entity.Project;
import ru.gasanov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class ProjectRepository {

    private List<Project> projects = new ArrayList<>();
    private final AtomicLong idGenerator = new AtomicLong();
    public List<Project> findAll() {
        return projects;
    }

    public void save(Project project) {
        if (project.getId() == null ){
            //pass
        } else{
            project.setId(idGenerator.incrementAndGet());
            projects.add(project);
        }
    }

    public Project create(final String name) {
        final Project project = new Project(name);
        projects.add(project);
        return project;
    }

    public void clear() {
        projects.clear();
    }

    public Project create(final String name, String description){
        final Project project = new Project(name);
        project.setName(name);
        project.setDescription(description);
        projects.add(project);
        return project;
    }

    public Project update(final Long id, final String name, String description){
        final Project project = findById(id);
        if (project == null) return null;
        project.setId(id);
        project.setDescription(description);
        project.setName(name);
        return project;
    }

    public Project findByIndex(int index){
        if  (index < 0 || index > projects.size() - 1 ) return null;
        return projects.get(index);
    }

    public Project findByName(final String name){
        if (name == null || name.isEmpty()) return null;
        for (final Project project: projects){
            if (project.getName().equals(name)) return project;
        }
        return null;
    }

    public Project removeByName(final String name){
        final Project project = findByName(name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public Project removeById(final Long id){
        final Project project = findById(id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public Project removeByIndex(final int index){
        final Project project = findByIndex(index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public Project findById(final Long id){
        if (id == 0) return null;
        for (final Project project: projects) {
            if(project.getId().equals(id)) return project;
        }
        return null;
    }

    public static void main(String[] args) {
        final ProjectRepository projectRepository = new ProjectRepository();
        projectRepository.create("Demo");
        projectRepository.create("Test");
        System.out.println(projectRepository.findAll());
        projectRepository.clear();
        System.out.println(projectRepository.findAll());
    }

    public int size() {
        return projects.size();
    }

    public List<Project> findByUserId(Long UserId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project: findAll()) {
            if (project.getId() == null) continue;
            if (project.getId().equals(UserId)) result.add(project);
        }
        return result;
    }
}
