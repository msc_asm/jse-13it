package ru.gasanov.tm.entity;

import java.util.Comparator;

public class User {
    private Long id;
    private String login;
    private String password;
    private UserRole role;

    public static Comparator<User> UserSortByLogin = new Comparator<User>() {
        @Override
        public int compare(User u1, User u2) {
            return u1.getLogin().compareTo(u2.getLogin());
        }
    };

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }
}