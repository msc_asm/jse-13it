package ru.gasanov.tm.controller;

import ru.gasanov.tm.entity.User;
import ru.gasanov.tm.entity.UserRole;
import ru.gasanov.tm.service.ProjectService;
import ru.gasanov.tm.service.TaskService;
import ru.gasanov.tm.service.UserService;

import java.util.List;

public class UserController extends AbstractController{
    private final UserService userService;
    private final TaskService taskService;
    private final ProjectService projectService;

    public UserController(UserService userService, TaskService taskService, ProjectService projectService) {
        this.userService = userService;
        this.taskService = taskService;
        this.projectService = projectService;
    }

    public int createUser(){
        System.out.println("[PLEASE ENTER USER PARAMETRS <login> <pass> <role>:");
        final String args = scanner.nextLine();
        String[] commandWithArg = args.split(" ");
        if (commandWithArg.length < 3) {
            System.out.println("Not enough arguments for creating");
            return 0;
        }
        try {
            String login = commandWithArg[0];
            String pass = commandWithArg[1];
            UserRole role = UserRole.valueOf(commandWithArg[2]);
            boolean result =  userService.createUser(login, pass,role);
            if (result){
                System.out.println("User created");
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Error! Invalid argument");
        }

        System.out.println("[OK]");
        return 0;
    }

    public int printUsers(){
        List<User> users = userService.findAll();
        if (users.size() == 0){
            System.out.println("No one user were found");
        }
        userService.UserSortByLogin(users);
        for (User user: users){
            System.out.println(
                    "Id: " + user.getId().toString() + " Login: " + user.getLogin() + " Role: " + user.getRole().toString()
            );
        }
        return 0;
    }

    public int clearUser() {
        userService.clearUsers();
        return 0;
    }

    public int getAvaliableTasks(){
        taskService.getAvaliableTasks();
        return 0;
    }

    public int getAvaliableProjects(){
        projectService.getAvaliableTasks();
        return 0;
    }
}
