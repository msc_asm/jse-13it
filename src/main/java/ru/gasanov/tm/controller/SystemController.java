package ru.gasanov.tm.controller;

import ru.gasanov.tm.App;
import ru.gasanov.tm.entity.Task;

import java.util.LinkedList;
import java.util.Queue;

public class SystemController {

    public static void displayWelcome() {
        System.out.println("* WELCOME TO TASK MANAGER *");
    }

    public int  displayAbout() {
        System.out.println("Gasanov Asiman");
        System.out.println("gasanov_ad@nlmk.com");
        return 0;
    }

    public  int displayError() {
        System.out.println("ERROR! You passed wrong parameters ");
        return -1;
    }

    public int  displayHelp() {
        System.out.println("version - Display program version.");
        System.out.println("about - Display programm info.");
        System.out.println("help - Display list of terminal commands.");
        System.out.println("exit - Terminate console.");
        System.out.println("project-list - Display list of projects.");
        System.out.println("project-clear - Clear list of projects.");
        System.out.println("project-create - Create project.");
        System.out.println("task-list - Display list of tasks.");
        System.out.println("task-clear - Clear list of tasks.");
        System.out.println("task-create - Create tasks.");
        System.out.println("project-view-by-index - View projects by index");
        System.out.println("project-view-by-id - View projects by id");
        System.out.println("project-remove-by-id - Remove project by id");
        System.out.println("project-remove-by-name  - Remove project by name");
        System.out.println("project-remove-by-index - Remove project by index");
        System.out.println("project-update-by-index - Update project by index");
        System.out.println("project-update-by-id - Update project by id");
        System.out.println("task-create  - Create task");
        System.out.println("task-clear - Clear list of tasks");
        System.out.println("task-list - Show list of tasks");
        System.out.println("task-view-by-index - View task by index");
        System.out.println("task-view-by-id - View task by id");
        System.out.println("task-remove-by-id - Remove task by id");
        System.out.println("task-remove-by-name - Remove task by name");
        System.out.println("task-remove-by-index - Remove task by id");
        System.out.println("task-update-by-index - Update task by index");
        System.out.println("task-update-by-id - Update task by id");
        System.out.println("task-set-user - Set user to task");
        System.out.println("task-detach-user - Detach user to task");
        System.out.println("project-set-user - Set user to project");
        System.out.println("project-detach-user - Detach user to project");
        System.out.println("project-set-user - <task_id> <user_id>");
        System.out.println("task-list-by-project-id - Display task list by project id.");
        System.out.println("task-add-to_project-by-ids - Add task to project by ids.");
        System.out.println("task-remove-from-project-by-ids - Remove task from project by ids.");
        System.out.println("user-create - Create user - <login> <pass> <role>");
        System.out.println("user-list - List user");
        System.out.println("user-clear - Clear user");
        System.out.println("login - Log in into system <login> <pass> login");
        System.out.println("logout - Log out of system");
        System.out.println("change-pass - Log in into system <oldpass> <newpass> login");
        System.out.println("change-user-pass - Log in into system <user_id> <pass> login");
        System.out.println("get-available-tasks - Show list of available tasks for user");
        System.out.println("get-available-project - Show list of available projects for user");
        System.out.println("history - show commands history");


        return 0;
    };

    public int displayExit() {
        System.out.println(" Terminate programm");
        return 0;
    };

    public int  displayVersion() {
        System.out.println("1.0.0");
        return 0;
    };

    public int displayHistory() {
        for (String str: App.history) {
            System.out.println(str);
        }
        return 0;
    }


}
