package ru.gasanov.tm.controller;

import ru.gasanov.tm.entity.UserRole;
import ru.gasanov.tm.service.AuthService;

import javax.swing.plaf.IconUIResource;

public class AuthController extends AbstractController{
    public final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    public int doLogin(){
        System.out.println("[PLEASE ENTER USER PARAMETERS <login> <pass> login:");
        final String args = scanner.nextLine();
        String[] commandWithArg = args.split(" ");
        if (commandWithArg.length < 3) {
            System.out.println("Not enough arguments for login");
        }

        String login = commandWithArg[0];
        String pass = commandWithArg[1];
        boolean success =  authService.doLogin(login, pass);
        if (success){
            System.out.println("logged in");
        } else {
            System.out.println("Something went wrong");
        }
        return 0;
    }

    public int doLogout(){
        authService.doLogout();
        return 0;
    }

    public int changePassword() {
        System.out.println("[PLEASE ENTER USER PARAMETERS <oldpass> <pass>:");
        final String args = scanner.nextLine();
        String[] commandWithArg = args.split(" ");
        if (commandWithArg.length < 2) {
            System.out.println("Not enough arguments to change password");
        }

        String oldpass = commandWithArg[0];
        String newpass = commandWithArg[1];
        boolean success =  authService.changeCurrentUserPassword(oldpass, newpass);
        if (success){
            System.out.println("password has changed");
        } else {
            System.out.println("Something went wrong");
        }
        return 0;
    }

    public int changeUserPassword() {
        System.out.println("[PLEASE ENTER USER PARAMETERS <login> <pass>:");
        final String args = scanner.nextLine();
        String[] commandWithArg = args.split(" ");
        if (commandWithArg.length < 2) {
            System.out.println("Not enough arguments to change password");
        }

        String login = commandWithArg[0];
        String newpass = commandWithArg[1];
        boolean success =  authService.changeCurrentUserPassword(login, newpass);
        if (success){
            System.out.println("password has changed for " + login);
        } else {
            System.out.println("Something went wrong");
        }
        return 0;
    }
}
