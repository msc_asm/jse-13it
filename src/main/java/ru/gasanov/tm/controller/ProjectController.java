package ru.gasanov.tm.controller;

import ru.gasanov.tm.entity.Project;
import ru.gasanov.tm.service.ProjectService;

public class ProjectController extends AbstractController{

    private final ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    public int assignTaskToUser() {
        System.out.println("[PLEASE ENTER USER PARAMETERS <project> <user>:");
        final String args = scanner.nextLine();
        String[] commandWithArg = args.split(" ");
        if (commandWithArg.length < 3) {
            System.out.println("Not enough arguments to set task");
        }
        try {
            Long projectId = Long.parseLong(commandWithArg[0]);
            Long userId = Long.parseLong(commandWithArg[1]);
            boolean success =  projectService.assignProjectToUser(userId, projectId);
            if (success){
                System.out.println("task has attached");
            } else {
                System.out.println("Something went wrong");
            }
        } catch (NumberFormatException e){
            System.out.println("Please, type numbers");
        }
        return 0;
    }

    public int detachProjectFromUser() {
        System.out.println("[PLEASE ENTER USER PARAMETERS <project> <user>:");
        final String args = scanner.nextLine();
        String[] commandWithArg = args.split(" ");
        if (commandWithArg.length < 3) {
            System.out.println("Not enough arguments to detach task");
        }
        try {
            Long projectId = Long.parseLong(commandWithArg[0]);
            Long userId = Long.parseLong(commandWithArg[1]);
            boolean success =  projectService.detachProjectToUser(userId, projectId);
            if (success){
                System.out.println("task has attached");
            } else {
                System.out.println("Something went wrong");
            }
        } catch (NumberFormatException e){
            System.out.println("Please, type numbers");
        }
        return 0;
    }
    public  int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("PLEASE ENTER PROJECT NAME");
        final String name = scanner.nextLine();
        projectService.create(name);
        System.out.println("[OK]");
        return 0;
    }

    public  int updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) -1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[PLEASE ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        System.out.println("[PLEASE ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int updateProjectById() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final Long id= Long.parseLong(scanner.nextLine());
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[PLEASE ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        System.out.println("[PLEASE ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("[PLEASE ENTER PROJECT INDEX:");
        final int index = scanner.nextInt() - 1;
        final Project project = projectService.removeByIndex(index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("[PLEASE ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        final Project project = projectService.removeByName(name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("[PLEASE ENTER PROJECT ID:");
        final Long id = scanner.nextLong();
        final Project project = projectService.removeById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }


    public int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

    public int viewProjectByIndex() {
        System.out.println("ENTER PROJECT INDEX:");
        final int index = scanner.nextInt() - 1;
        final Project project = projectService.findByIndex(index);
        viewProject(project);
        return 0;
    }

    public int viewProjectById() {
        System.out.println("ENTER PROJECT ID:");
        final Long id = scanner.nextLong();
        final Project project = projectService.findById(id);
        viewProject(project);
        return 0;
    }

    public int viewProjectByName() {
        System.out.println("ENTER PROJECT NAME:");
        final Long name = scanner.nextLong();
        final Project project = projectService.findById(name);
        viewProject(project);
        return 0;
    }

    public int listProject() {
        System.out.println("[LIST PROJECT]");
        int index = 1;
        for (final Project project: projectService.findAll()) {
            System.out.println(index + ". PROJECTID: " + project.getId() + "; NAME: " + project.getName() + "; DESCRIPTION: " + project.getDescription());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

}