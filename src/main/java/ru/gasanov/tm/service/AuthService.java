package ru.gasanov.tm.service;

import org.apache.commons.codec.digest.DigestUtils;
import ru.gasanov.tm.entity.User;
import ru.gasanov.tm.entity.UserRole;
import ru.gasanov.tm.repository.UserRepository;

public class AuthService {
    private UserRepository userRepository;
    private User currentUser = null;

    public AuthService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean doLogin(String login, String pass){
        User user = userRepository.findByLogin(login);
        if (user != null && user.getPassword().equals(DigestUtils.md5(pass))){
            currentUser = user;
            return true;
        }
        return false;
    }

    public boolean changeCurrentUserPassword(String oldpass, String newpass){
        if (currentUser == null){
            return false;
        }

        if (currentUser.getPassword().equals(DigestUtils.md5Hex(oldpass))){
            currentUser.setPassword(DigestUtils.md5Hex(newpass));
            userRepository.save(currentUser);
            return true;
        }
        return false;
    }

    public boolean changeUserPassword(String login, String newpass){
        if (currentUser == null){
            return false;
        }

        if (currentUser.getRole() != UserRole.ADMIN){
            return false;
        }

        User user = userRepository.findByLogin(login);
        if (user == null){
            return false;
        }

        user.setPassword(DigestUtils.md5Hex(newpass));
        userRepository.save(user);
        return true;
    }

    public void doLogout(){
        currentUser = null;
    }

    public User getCurrentUser() {
        return currentUser;
    }
}
