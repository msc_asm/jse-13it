package ru.gasanov.tm.service;

import ru.gasanov.tm.entity.User;
import ru.gasanov.tm.entity.UserRole;
import ru.gasanov.tm.repository.UserRepository;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.Collections;
import java.util.List;

public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean createUser(String login, String pass, UserRole role){
        if (userRepository.findByLogin(login) != null) {
            return false;
        }
        User user = new User();
        user.setLogin(login);
        user.setPassword(DigestUtils.md5Hex(pass));
        user.setRole(role);
        userRepository.save(user);
        return true;
    }

    public List<User> UserSortByLogin(List<User> users) {
        Collections.sort(users, User.UserSortByLogin);
        return users;
    }


    public List<User> findAll(){
        return userRepository.findAll();
    }

    public void clearUsers(){
        userRepository.deleteAll();
    }
}
