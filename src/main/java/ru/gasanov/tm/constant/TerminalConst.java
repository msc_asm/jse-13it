package ru.gasanov.tm.constant;

public class TerminalConst {
    public static final String CMD_HELP = "help";
    public static final String CMD_VERSION = "version";
    public static final String CMD_ABOUT = "about";
    public static final String CMD_EXIT = "exit";

    public static final String PROJECT_CREATE = "project-create";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PROJECT_LIST = "project-list";
    public static final String PROJECT_VIEW_BY_INDEX = "project-view-by-index";
    public static final String PROJECT_VIEW_BY_ID = "project-view-by-id";
    public static final String PROJECT_VIEW_BY_NAME = "project-view-by-name";
    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";
    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";
    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    public static final String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";
    public static final String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_LIST = "task-list";
    public static final String TASK_VIEW_BY_INDEX = "task-view-by-index";
    public static final String TASK_VIEW_BY_ID = "task-view-by-id";
    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";
    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";
    public static final String TASK_REMOVE_BY_INDEX = "task-remove-by-index";
    public static final String TASK_UPDATE_BY_INDEX = "task-update-by-index";
    public static final String TASK_UPDATE_BY_ID = "task-update-by-id";
    public static final String TASK_LIST_FROM_PROJECT_BY_IDS = "task-list-by-project";
    public static final String TASK_ADD_TO_PROJECT_BY_IDS = "task-add-to-project";
    public static final String TASK_DELETE_FROM_PROJECT_BY_IDS = "task-delete-from-project";

    public static final String USER_CREATE = "user-create";
    public static final String USER_LIST = "user-list";
    public static final String USER_CLEAR = "user-clear";
    public static final String TASK_SET_USER = "user-set-task";
    public static final String TASK_DETACH_USER = "user-detach-task";

    public static final String PROJECT_SET_USER = "user-set-project";
    public static final String PROJECT_DETACH_USER = "user-detach-project";

    public static final String AVAILABLE_PROJECTS_FOR_USER = "user-list-available-projects";
    public static final String AVAILABLE_TASKS_FOR_USER = "user-list-available-tasks";

    public static final String AUTH_LOGIN = "auth-login";
    public static final String AUTH_LOGOUT= "auth-logout";
    public static final String CHANGE_PASS= "auth-change-password";
    public static final String CHANGE_USER_PASS = "auth-change-password-for-user";
    public static final String REMOVE_PROJECT_BY_ID_WITH_TASKS = "delete-project-and-all-related-tasks";

    public static final String HISTORY = "history";
}
