package ru.gasanov.tm;

import ru.gasanov.tm.controller.*;
import ru.gasanov.tm.repository.ProjectRepository;
import ru.gasanov.tm.repository.TaskRepository;
import ru.gasanov.tm.repository.UserRepository;
import ru.gasanov.tm.service.*;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import static ru.gasanov.tm.constant.TerminalConst.*;

public class App {
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final TaskRepository taskRepository = new TaskRepository();
    private final UserRepository userRepository = new UserRepository();

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final UserService userService = new UserService(userRepository);
    private final AuthService authService = new AuthService(userRepository);
    private final TaskService taskService = new TaskService(taskRepository, userRepository, authService);
    private final ProjectService projectService = new ProjectService(projectRepository, userRepository, authService);

    private final ProjectController projectController = new ProjectController(projectService);
    private final TaskController taskController = new TaskController(taskService, projectTaskService);
    private final SystemController systemController = new SystemController();
    private final UserController userController = new UserController(userService, taskService, projectService);
    private final AuthController authController = new AuthController(authService);
    public static LinkedList<String> history = new LinkedList<>();
    public static final int MAX_HISTORY_SIZE = 10;

    {
        projectRepository.create("Demo project #1", "My first project");
        projectRepository.create("Demo project #2", "My second project");
        taskRepository.create("Demo task #1","My first task");
        taskRepository.create("Demo task #2","My second task");

    }

    public static void main(final String[] args) {
        final App app = new App();
        app.run(args);
        app.systemController.displayWelcome();
        final Scanner scanner = new Scanner(System.in);
        String command  = "";
        while (!CMD_EXIT.equals(command)){
            command = scanner.nextLine();
            history.add(command);
            if (history.size() > 10) history.pollFirst();
            app.run(command);
        }
    }

    public void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    public int run(final String param) {
        if (param == null) return -1;
        switch (param) {
            case CMD_HELP: return systemController.displayHelp();
            case CMD_ABOUT: return systemController.displayAbout();
            case CMD_VERSION: return systemController.displayVersion();
            case CMD_EXIT: return systemController.displayExit();

            case PROJECT_LIST: return projectController.listProject();
            case PROJECT_VIEW_BY_INDEX: return projectController.viewProjectByIndex();
            case PROJECT_VIEW_BY_ID: return projectController.viewProjectById();
            case PROJECT_VIEW_BY_NAME: return projectController.viewProjectByName();
            case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX: return projectController.updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID: return projectController.updateProjectById();
            case PROJECT_CLEAR: return projectController.clearProject();
            case PROJECT_CREATE: return projectController.createProject();

            case TASK_LIST: return taskController.listTask();
            case TASK_VIEW_BY_INDEX: return taskController.viewTaskByIndex();
            case TASK_VIEW_BY_ID: return taskController.viewTaskById();
            case TASK_REMOVE_BY_NAME: return taskController.removeTaskByName();
            case TASK_REMOVE_BY_ID: return taskController.removeTaskById();
            case TASK_REMOVE_BY_INDEX: return taskController.removeTaskByIndex();
            case TASK_UPDATE_BY_INDEX: return taskController.updateTaskByIndex();
            case TASK_UPDATE_BY_ID: return taskController.updateTaskById();
            case TASK_CLEAR: return taskController.clearTask();
            case TASK_CREATE: return taskController.createTask();

            case TASK_SET_USER: return taskController.assignTaskToUser();
            case TASK_DETACH_USER: return taskController.detachTaskFromUser();

            case PROJECT_SET_USER: return projectController.assignTaskToUser();
            case PROJECT_DETACH_USER: return projectController.detachProjectFromUser();

            case REMOVE_PROJECT_BY_ID_WITH_TASKS: return taskController.removeProjectByIdWithTasks();
            case TASK_ADD_TO_PROJECT_BY_IDS: return taskController.addTaskToProjectByIds();
            case TASK_DELETE_FROM_PROJECT_BY_IDS: return taskController.removeTaskFromProjectByIds();
            case TASK_LIST_FROM_PROJECT_BY_IDS: return taskController.listTaskByProjectId();

            case USER_CLEAR: return userController.clearUser();
            case USER_CREATE: return userController.createUser();
            case USER_LIST: return userController.printUsers();

            case AUTH_LOGIN: return authController.doLogin();
            case AUTH_LOGOUT: return authController.doLogout();
            case CHANGE_PASS: return authController.changePassword();
            case CHANGE_USER_PASS: return authController.changeUserPassword();

            case AVAILABLE_TASKS_FOR_USER: return userController.getAvaliableTasks();
            case AVAILABLE_PROJECTS_FOR_USER: return userController.getAvaliableProjects();

            case HISTORY: return systemController.displayHistory();

            default: return systemController.displayError();
        }
    }

}
